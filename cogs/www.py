"""
www-bot
Copyright (C) 2020  John Meow

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import annotations

import asyncio
import re
from asyncio import TimeoutError
from dataclasses import dataclass
from typing import Optional, TypeVar, Union

from aiohttp import ClientSession
from discord import Embed, Message
from discord.embeds import _EmptyEmbed
from discord.ext import commands
from lxml import objectify

PIC_REGEX = re.compile(r"\(pic: ([\w.]+)\)")

T = TypeVar("T")

EmbedOptional = Union[T, _EmptyEmbed]


def none_map(val: Optional[T]) -> EmbedOptional[T]:
    return Embed.Empty if val is None else val


@dataclass(frozen=True)
class Question:
    """Question dataclass"""

    answer: str
    author: str
    comment: str
    id: int
    number: int
    parent_id: str
    pass_criteria: str
    question: str
    src: str
    question_pic: Optional[str] = None
    comment_pic: Optional[str] = None
    complexity: Optional[int] = None
    base_url: str = "https://db.chgk.info"

    @classmethod
    def from_element(cls, elem: objectify.Element):
        """Creates question from lxml element"""
        text = str(elem.Question)
        match = PIC_REGEX.match(text)
        if match is not None:
            question_pic: Optional[str] = match.group(1)
            text = PIC_REGEX.sub("", text, count=1)
        else:
            question_pic = None

        comment = str(elem.Comments)
        match = PIC_REGEX.match(comment)
        if match is not None:
            comment_pic: Optional[str] = match.group(1)
            comment = PIC_REGEX.sub("", comment, count=1)
        else:
            comment_pic = None
        complexity: Optional[int] = (
            int(elem.Complexity) if elem.Complexity else None
        )

        return cls(
            answer=str(elem.Answer),
            author=str(elem.Authors),
            comment=comment,
            id=int(elem.QuestionId),
            number=int(elem.Number),
            parent_id=str(elem.ParentTextId),
            pass_criteria=str(elem.PassCriteria),
            question=text,
            src=str(elem.Sources),
            complexity=complexity,
            question_pic=question_pic,
            comment_pic=comment_pic,
        )

    @property
    def url(self) -> str:
        """Returns question URL"""
        return f"{self.base_url}/question/{self.parent_id}/{self.number}"

    @property
    def question_image_url(self) -> Optional[str]:
        """Returns image URL"""
        if self.question_pic is None:
            return None
        return f"{self.base_url}/images/db/{self.question_pic}"

    @property
    def comment_image_url(self) -> Optional[str]:
        """Returns image URL"""
        if self.comment_pic is None:
            return None
        return f"{self.base_url}/images/db/{self.comment_pic}"

    @property
    def question_embed(self) -> Embed:
        """Creates Discord embed with question"""
        e = (
            Embed(
                title=f"Вопрос №{self.id}",
                description=self.question,
                url=self.url,
                color=0xE91F63,
            )
            .set_author(
                name=self.author,
            )
            .set_footer(
                text="Вопросы распространяются согласно условиям лицензии "
                "https://db.chgk.info/copyright"
            )
        )
        if self.complexity:
            e.add_field(name="Сложность", value=str(self.complexity))
        return e.set_image(url=none_map(self.question_image_url))

    @property
    def full_embed(self) -> Embed:
        """Creates Discord embed with all information, including answer"""
        e = self.question_embed.add_field(name="Ответ", value=self.answer)
        if self.pass_criteria:
            e.add_field(name="Зачёт", value=self.pass_criteria)
        if self.comment:
            e.add_field(name="Комментарий", value=self.comment)
        if self.src:
            e.add_field(name="Источник", value=self.src)
        if self.comment_image_url:
            e.set_image(url=self.comment_image_url)
        return e


class WWWCog(commands.Cog):
    db: dict[Optional[int], set[Question]]  # type: ignore
    session: ClientSession

    def __init__(self):
        self.db = {
            1: set(),
            2: set(),
            3: set(),
            4: set(),
            5: set(),
            None: set(),
        }

    @commands.command(aliases=["up", "fetch"])
    async def update(
        self, ctx: commands.Context, complexity: Optional[int] = None
    ):
        """Обновляет базу вопросов"""
        async with ctx.channel.typing():
            msg: Message = await ctx.send("Обновляю базу вопросов…")
            await self.update_db(complexity=complexity)
        await msg.edit(content="Обновлено!")

    @commands.Cog.listener()
    async def on_ready(self):
        self.session = ClientSession()

    async def update_db(self, complexity: Optional[int] = None):
        """Updates question database"""
        async with self.session.get(
            "https://db.chgk.info/xml/random/{}answers/".format(
                "" if complexity is None else f"complexity{complexity}/"
            )
        ) as resp:
            loop = asyncio.get_event_loop()
            xml: objectify.Element = await loop.run_in_executor(
                None, objectify.fromstring, await resp.read()
            )
        self.db[complexity] |= set(
            map(Question.from_element, xml.getchildren())
        )

    @commands.command(aliases=["q", "next"])
    async def question(
        self,
        ctx: commands.Context,
        complexity: Optional[int] = None,
        time: int = 80,
    ):
        """Присылает вопрос, через минуту присылает ответ на него"""
        if not self.db[complexity]:
            await ctx.send("Вопросы в базе кончились, получаю новые…")
            await self.update(ctx, complexity)
        question = self.db[complexity].pop()
        await ctx.send(
            f"Загадка от {ctx.bot.user.mention}, на размышление даётся {time} секунд",  # noqa: E501
            embed=question.question_embed,
        )

        def check(msg: Message) -> bool:
            return msg.content == "skip" and msg.channel == ctx.channel

        try:
            await ctx.bot.wait_for("message", check=check, timeout=time - 10)
        except TimeoutError:
            pass
        else:
            return await ctx.send(
                "Досрочный ответ!", embed=question.full_embed
            )
        await ctx.send("Осталось 10 секунд!")

        try:
            await ctx.bot.wait_for("message", check=check, timeout=10)
        except TimeoutError:
            pass
        else:
            return await ctx.send(
                "Досрочный ответ!", embed=question.full_embed
            )
        await ctx.send("Время вышло!", embed=question.full_embed)


def setup(bot: commands.Bot):
    bot.add_cog(WWWCog())
